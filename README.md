# Impostor systemd Service

systemd service file for the unofficial Among Us dedicated server, Impostor: https://github.com/AeonLucid/Impostor

## Setup

Either add the [`config.json` file](https://github.com/AeonLucid/Impostor/blob/master/src/Impostor.Server/config.json) to `/etc/impostor/` or modify the directory in the service file to point to where your config file is.

Modify the `PublicIp` field to the machine's public IP.

Copy the `impostor.service` file into `/etc/systemd/system/impostor.service`.

Enable + start the impostor service.

```
# systemctl enable impostor.service
# systemctl start impostor.service
```
